20软件工程4班 202010211232 张树杰

# 实验1：SQL语句的执行计划分析与优化指导

## 实验目的

  分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

### 实验步骤

- 用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

```text
 v_$sesstat, v_$statname 和 v_$session
```

- 权限分配过程如下

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
```

查询1：查询员工工资高于2000的员工信息和所在部门信息

```SQL
set autotrace on
SELECT employees.employee_id, employees.first_name, employees.last_name, departments.department_name
FROM employees
INNER JOIN departments ON employees.department_id = departments.department_id
WHERE employees.salary >2000  order by  departments.department_name;;

统计信息：
----------------------------------------------------------
	  9  recursive calls
	  0  db block gets
	 23  consistent gets
	  0  physical reads
	  0  redo size
       4635  bytes sent via SQL*Net to client
	685  bytes received via SQL*Net from client
	  9  SQL*Net roundtrips to/from client
	  1  sorts (memory)
	  0  sorts (disk)
	106  rows processed


输出结果：
Predicate Information (identified by operation id):
---------------------------------------------------

   4 - access("EMPLOYEES"."DEPARTMENT_ID"="DEPARTMENTS"."DEPARTMENT_ID")
       filter("EMPLOYEES"."DEPARTMENT_ID"="DEPARTMENTS"."DEPARTMENT_ID")
   5 - filter("EMPLOYEES"."SALARY">2000)


```

- 查询2

```SQL
set autotrace on
SELECT employees.employee_id, employees.first_name, employees.last_name, departments.department_name
FROM employees, departments
WHERE employees.department_id = departments.department_id
AND employees.salary > 2000  order by  departments.department_name;;

统计信息
----------------------------------------------------------
	  8  recursive calls
	  0  db block gets
	 21  consistent gets
	  0  physical reads
	  0  redo size
       4635  bytes sent via SQL*Net to client
	685  bytes received via SQL*Net from client
	  9  SQL*Net roundtrips to/from client
	  1  sorts (memory)
	  0  sorts (disk)
	106  rows processed

输出结果：
Predicate Information (identified by operation id):
---------------------------------------------------

   4 - access("EMPLOYEES"."DEPARTMENT_ID"="DEPARTMENTS"."DEPARTMENT_ID")
       filter("EMPLOYEES"."DEPARTMENT_ID"="DEPARTMENTS"."DEPARTMENT_ID")
   5 - filter("EMPLOYEES"."SALARY">2000)


```

## 分析

通过统计信息的观察，第一句SQL执行效率会高一点。



## 实验注意事项

- 完成时间：2023-3-21，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test1目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
